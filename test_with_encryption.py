#USER runs this script
import csv
import time
import numpy
from encryption import enc
from decryption import dec
from convert_dec_res2float import dec_res2float
from convert_input import float2int_input
from convert_weight import float2int_weight, wih
from server import neuralNetwork
from split_weights import array2list, split_we
test_data_file = open("mnist_dataset/mnist_test.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()
scorecard = []
f = open('csvFile/enc_time.csv', 'w')
wr_e = csv.writer(f)
d = open('csvFile/dec_time.csv', 'w')
wr_d = csv.writer(d)
r = open('csvFile/performance.csv', 'w')
wr = csv.writer(r)
counter = 0

def test_encrptedNetwork(hiddennodes,fac_in,factor_weight,num_deci,imag_numb):
    counter=1

    encryption_time_list = []
    decryption_time_list = []
    image_counter = 0
    #you can choose the number of test Images
    for record in test_data_list[0:imag_numb]:
        image_counter=image_counter+1
        print("image_counter",image_counter)

        all_values = record.split(',')

        inputs_list = (numpy.asfarray(all_values[1:]) / 250.0 * 0.99) + 0.01
        print(inputs_list)
        inputs = numpy.array(inputs_list)
        # convert to int
        int_weight= float2int_weight(wih, factor_weight,num_deci)
        test = array2list(int_weight)
        split_weight = split_we(test)

        int_inputs = float2int_input(inputs, fac_in, num_deci)
        enc_tic = time.perf_counter()
        #encryption
        enc_list = enc(split_weight, int_inputs,hiddennodes)
        enc_toc = time.perf_counter()
        encryption_time = enc_toc - enc_tic
        encryption_time_list.append(encryption_time)
        dec_tic=time.perf_counter()
        #decryption
        decryption_data=dec(enc_list,hiddennodes)
        dec_toc=time.perf_counter()
        decryption_time=dec_toc-dec_tic
        decryption_time_list.append(decryption_time)
        #print("decryption_time",decryption_time)
        #convert to float
        int2float_res = dec_res2float(decryption_data, fac_in*factor_weight)

        #test the Network
        # number of input, hidden and output nodes
        input_nodes = 784
        hidden_nodes = 40
        output_nodes = 10

        # learning rate
        learning_rate = 0.1

        # create instance of neural network
        n = neuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)

        # correct answer is first value
        correct_label = int(all_values[0])
        #convert to float after decryption
        s_float= numpy.asfarray(int2float_res)
        # test the network
        outputs = n.query_enc(s_float)
        # the index of the highest value corresponds to the label
        label = numpy.argmax(outputs)

        # append correct or incorrect to list
        if (label == correct_label):
            # network's answer matches correct answer, add 1 to scorecard
            scorecard.append(1)
        else:
            # network's answer doesn't match correct answer, add 0 to scorecard
            scorecard.append(0)


        # calculate the performance score, the fraction of correct answers
        scorecard_array = numpy.asarray(scorecard)
        performance =  scorecard_array.sum() / scorecard_array.size

        #print("performance = ", scorecard_array.sum() / scorecard_array.size)
        #save encryption_data in data_with_enc.csv
    #print("factor_weight",factor_weight)
    wr_e.writerow(encryption_time_list)
    wr_d.writerow(decryption_time_list)
    counter = (counter + 1) * counter
    print(decryption_time_list)
    wr.writerow((("performance:", performance, "factor_weight:", factor_weight)))

    return performance

performance_list=[]


#with factors_weigts can you  decide the numer of decimal places Ex 10 is one decimal place, 100 is 2 decimal places...etc
# fac_in is for the scaling of inputsdata in test is always 1


test_encrptedNetwork(40,10,10,1,300)
test_encrptedNetwork(40,10,100,1,300)
test_encrptedNetwork(40,1,1000,1,300)
test_encrptedNetwork(40,1,10000,1,300)