#!/usr/bin/env python3
#INVOKE EXTENSION MODULE
import FEcrypt

#GENERATE CONTEXT
my_context=FEcrypt.create_context(3,10,2048)
print("Our context is:")
print(FEcrypt.context_to_string(my_context))
print("")

#KEY GENERATION
[secret_key, public_key]=FEcrypt.keygen(my_context)
print("Secret key is:")
print(FEcrypt.vector_to_string(secret_key))
print("Public key is:")
print(FEcrypt.vector_to_string(public_key))


#Generate a Evaluation Key for function 1,2,3
weights=[1,2,3]
weight_vector=FEcrypt.create_mpz_vector(weights)
print("Our function is:")
print(FEcrypt.vector_to_string(weight_vector))

evaluation_key=FEcrypt.generate_evaluation_key(my_context, weight_vector, secret_key)
print("Evaluation key is:")
print(FEcrypt.number_to_string(evaluation_key))



#Now encrypt some data
data=[4,5,6]
data_vector=FEcrypt.create_mpz_vector(data)
print("Our data is:")
print(FEcrypt.vector_to_string(data_vector))

ciphertext=FEcrypt.encrypt(my_context, public_key, data_vector)
print("Encrypted data:")
print(FEcrypt.vector_to_string(ciphertext))
#Decrypt result, we expect: 1*4+2*5+3*6=32
result = FEcrypt.decrypt(ciphertext, weight_vector, my_context, evaluation_key)
print("OUR RESULT IS:")
print (result)
