import numpy

from server import neuralNetwork

scorecard=[]
# load the mnist test data CSV file into a list
test_data_file = open("mnist_dataset/mnist_test.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()
n=neuralNetwork(784,40,10,0.01)
def test_Network(n):
    for record in test_data_list:
        # split the record by the ',' commas
        all_values = record.split(',')
        # scale and shift the inputs
        inputs = (numpy.asfarray(all_values[1:]) / 250.0 * 0.99) + 0.01
        output=n.query(inputs)
        correct_label = int(all_values[0])
        #print("correct_label", correct_label)
        label = numpy.argmax(output)
        #print("label", label)

        # append correct or incorrect to list
        if (label == correct_label):
            # network's answer matches correct answer, add 1 to scorecard
            scorecard.append(1)
        else:
            # network's answer doesn't match correct answer, add 0 to scorecard
            scorecard.append(0)

        # calculate the performance score, the fraction of correct answers
        scorecard_array = numpy.asarray(scorecard)
        #print("scorecard", len(scorecard))
        performance = scorecard_array.sum() / scorecard_array.size

    #print("performance = ", scorecard_array.sum() / scorecard_array.size)
    return performance



test_Network(n)