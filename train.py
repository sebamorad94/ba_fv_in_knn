#prepare the network
import csv

import pickle
import numpy


from test_without_encryption import test_Network
performance_with_diff_lernrate= open('csvFile/performance_with_diff_lernrate.csv', 'w')
wr = csv.writer(performance_with_diff_lernrate)

from server import neuralNetwork



Network_list=[]

def train_the_Network(inputnodes, hiddennodes, outputnodes, learningrate,epochs):

        n = neuralNetwork(inputnodes, hiddennodes, outputnodes, learningrate)

        # load the mnist training data CSV file into a list
        training_data_file = open("mnist_dataset/mnist_train.csv", 'r')
        training_data_list = training_data_file.readlines()
        training_data_file.close()
        # train the neural network
        # epochs is the number of times the training data set is used for training
        for e in range(epochs):

            # go through all records in the training data set
            for record in training_data_list:
                # split the record by the ',' commas
                all_values = record.split(',')
                # scale and shift the inputs
                inputs = (numpy.asfarray(all_values[1:]) / 250.0 * 0.99) + 0.01

                # create the target output values (all 0.01, except the desired label which is 0.99)
                targets = numpy.zeros(10) + 0.01
                # all_values[0] is the target label for this record
                targets[int(all_values[0])] = 0.99
                n.train(inputs, targets)
                with open("wih.p", "wb") as file:
                    pickle.dump(n.wih, file)
                file.close()

                with open("who.p", "wb") as file:
                    pickle.dump(n.who, file)
                file.close()

        return (n)


def plot_lr(lernrate_list, ne):
        acc_list=[]
        for lr in lernrate_list:
            n = train_the_Network(784, ne, 10, lr, 1)
            acc = test_Network(n)
            acc_list.append(acc)
        wr.writerow(acc_list)
        print("acc_list", acc_list)

        return acc_list
neurnlist=[10,20,40,60,80,100]
lernrate_list=[0.001,0.01,0.1]
def main():
    for ne in neurnlist:
        plot_lr(lernrate_list, ne)
        print(plot_lr(lernrate_list, ne))
    pass



if __name__ == "__main__":
    main()


