import statistics
import matplotlib.pyplot as plt
import numpy

#mean of
def get_mean(l):
    test=[statistics.mean(list_l) for list_l in l]
    return test
enc_lists=[]
#load encryption times from csv file
def enc_lists_gen():
    encryption_time_file = open("../csvFile/enc_time.csv", 'r')
    encryption_time_list = encryption_time_file.readlines()
    encryption_time_file.close()
    for time_list in encryption_time_list:
        time_lists = time_list.split(',')
        time_lists= numpy.asfarray(time_lists)
        time_lists=time_lists.tolist()
        enc_lists.append(time_lists)
    return enc_lists
# save in list
enc_lists=enc_lists_gen()
#mean of encryption times for  certain Numer of Decimal places
enc_lists_mean=get_mean(enc_lists)
#Numer of Decimal places
ns=[1,2,3,4]
plt.ylabel("benötigte Zeit für Verschlüsselung in Sekunden",fontsize = 25)
plt.plot(ns, enc_lists_mean)
plt.scatter(ns, enc_lists_mean)
plt.xticks(ns,fontsize = 20)
plt.yticks(enc_lists_mean,fontsize = 20)
plt.xlabel("Anzahl von Nachkommastellen",fontsize = 25)
plt.title("Verschlüsselungszeit pro Bild",fontsize = 25)
#plt.savefig("graphs/encryption_time_of_the_Network__with_Decimal_places.png")
plt.show()