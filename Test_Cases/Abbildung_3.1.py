
import matplotlib.pyplot as plt
import numpy


lernrate_list=[0.001,0.01,0.1]
neuron_list = [10,20,40,60,80,100]
performance_lists=[]
def main():
    performance_with_diff_lernrate_file = open("../csvFile/performance_with_diff_lernrate.csv", 'r')
    performance_with_diff_lernrate_list = performance_with_diff_lernrate_file.readlines()
    performance_with_diff_lernrate_file.close()
    for performance in range(len(performance_with_diff_lernrate_list)):
        performance_list = performance_with_diff_lernrate_list[performance].split(',')
        performance_list = numpy.asfarray(performance_list)
        performance_list = performance_list.tolist()
        plt.plot(lernrate_list,performance_list)


        neur = str(neuron_list[performance])
        plt.plot(lernrate_list,performance_list, label='mit' + neur + 'Neuronen')
        plt.scatter(lernrate_list, performance_list)

        plt.xticks(lernrate_list)
        plt.yticks([0.90,0.95,0.96])
        plt.legend()
        plt.ylabel("Genauigkeit in Prozent")
        plt.xlabel("lernrate")
        plt.title("beste Lernrate vs Neuronen Anzahl der Eingabeschicht")
        #plt.savefig("graphs/train_the_Network.png")


    plt.show()
if __name__ == "__main__":
    main()

