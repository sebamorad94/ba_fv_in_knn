
import matplotlib.pyplot as plt
import numpy
dec_lists_mean=[]
ns=[1,2,3,4]
decryption_time_file = open("../csvFile/dec_time.csv", 'r')
decryption_time_list = decryption_time_file.readlines()
decryption_time_file.close()

dec_lists_mean=[21.42, 24.57,26.24,33.17]

plt.ylabel("benötigte Zeit für Entschlüsselung in Sekunden",fontsize = 25)
plt.plot(ns, dec_lists_mean)
plt.scatter(ns, dec_lists_mean)

plt.xticks(ns,fontsize = 20)
plt.yticks(dec_lists_mean,fontsize = 20)
plt.xlabel("Anzahl von Nachkommastellen",fontsize = 25)
plt.title("Entschlüsselungszeit pro Bild",fontsize = 25)

#plt.savefig("graphs/encryption_time_of_the_Network__with_Decimal_places.png")
plt.show()