#Testfall: Das Netz mit verschlüsselten Daten testen

import matplotlib.pyplot as plt
import numpy

nach_kommastellen=[1,2,3,4]
performance_lists=[]
performance_file = open("../csvFile/performance.csv", 'r')
performance_list = performance_file.readlines()
performance_file.close()
for performance in performance_list:
    performance_list = numpy.asfarray(performance_list)
    performance_list = performance_list.tolist()
    plt.plot(nach_kommastellen,performance_list)
    plt.scatter(nach_kommastellen, performance_list)
    plt.xticks(nach_kommastellen,fontsize = 20)
    plt.yticks([0.50,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.90,0.95,0.99],fontsize = 20)
    plt.ylabel("Genauigkeit in prozent",fontsize = 25)
    plt.xlabel("Anzahl an Nachkommastellen",fontsize = 25)
    plt.title(" Genauigkeit Abhängig von Anzahl an Nachkommastellen",fontsize = 25)
    #plt.savefig("graphs/test_the enc_Network_with_Decimal_places.png")
plt.show()