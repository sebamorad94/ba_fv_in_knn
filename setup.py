"""
This file is used to compile the extension from within the IDE
"""
import sys
sys.argv.append("build_ext")
sys.argv.append("--inplace")
from setuptools import setup, Extension
from Cython.Build import cythonize


# First create an Extension object with the appropriate name and sources
ext = Extension(name="FEcrypt", sources=["c_src/py_fer_interface.c", "c_src/enc_decrypt.c","c_src/helpers.c","c_src/keygen.c", "wrap_pyfe.pyx"], libraries=["gmp","sodium"], include_dirs=["./c_includes"])

# Use cythonize on the extension object.
setup(ext_modules=cythonize(ext, language_level=3))

#Cleanup
import os
os.remove("wrap_pyfe.c")
os.system("rm -rf build")