import csv
import statistics
import time
import FEcrypt
import matplotlib.pyplot as plt
from encryption import greate_context

durchläufe=20
def keyGen_Time_test(keyLength,durchläufe):

    keyLength_list = []
    laufZeit_list = []
    keyLength_list.append(keyLength)
    for i in range(durchläufe):
        tic = time.perf_counter()
        my_context = greate_context(keyLength)
        [secret_key, public_key] = FEcrypt.keygen(my_context)
        toc = time.perf_counter()
        time_for_KeyGen = toc - tic
        laufZeit_list.append(time_for_KeyGen)

    return laufZeit_list


keyLength_lists = []
laufZeit_lists = []

def get_median(l):
    test = [statistics.median(list_l) for list_l in l]
    return test
def get_mean(l):
    test=[statistics.mean(list_l) for list_l in l]
    return test
keyLength_list=[]


f = open('csvFile/save_time.csv', 'w')
keyLength= 64
for i in range(6):
    mw_time=[]
    keyLength=keyLength*2
    keyLength_list.append(keyLength)
    zeit =keyGen_Time_test(keyLength,durchläufe)
    laufZeit_lists.append(zeit)
    mw_time=get_mean(laufZeit_lists)
    print("mw_time",mw_time)
    wr = csv.writer(f)
    wr.writerow((("zeitlist:", zeit, "Mittelwert:",mw_time[i], "key_length:",keyLength)))


    plt.xlabel('Schlüssellänge in  Bits',fontsize = 20)
    plt.ylabel('benötigte Zeit zu Schlüsselgenerierung in Sekunden',fontsize = 20)
    d=str(durchläufe)
    plt.title("Durchschnitt von"+d+"Durchläufe",fontsize = 20)
    plt.xticks([128,1024,2048,4096,8192],fontsize = 20)

    plt.plot(keyLength_list,mw_time)
    plt.plot(keyLength_list, mw_time,"bo")

    plt.yticks(mw_time,fontsize = 20)
    plt.legend()
    s=str(i)
wr.writerow(("Mittelwert_list:",mw_time))
wr.writerow(("keyLength_list", keyLength_list))

plt.show()
#plt.savefig("graphs/pu_pr_Keys_Generation_time.png",dpi=100)




