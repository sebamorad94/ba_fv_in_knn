
import FEcrypt
from convert_weight import weight_sum

def greate_context(keyLength):
    s = weight_sum()
    n = s * 250
    my_context = FEcrypt.create_context(784, n, keyLength)
    return my_context
#data encryption
def enc(split_weight, int_inputs,hiddennodes):
    contex = []
    #my_context
    my_context = greate_context(128)
    contex.append(my_context)
    [secret_key, public_key] = FEcrypt.keygen(my_context)
    weights_list = split_weight
    weights_list_pos = []
    weights_list_neg = []
    cipher_list_pos = []
    cipher_list_neg = []
    ev_pos = []
    ev_neg = []
    for w in range(0, hiddennodes):
        weights = weights_list[w][0]
        weight_vector = FEcrypt.create_mpz_vector(weights)
        weights_list_pos.append(weight_vector)
        evaluation_key = FEcrypt.generate_evaluation_key(my_context, weight_vector, secret_key)
        ev_pos.append(evaluation_key)
        data = int_inputs

        data_vector = FEcrypt.create_mpz_vector(data)
        ciphertext = FEcrypt.encrypt(my_context, public_key, data_vector)
        cipher_list_pos.append(ciphertext)


        weights = weights_list[w][1]
        weight_vector = FEcrypt.create_mpz_vector(weights)
        weights_list_neg.append(weight_vector)

        evaluation_key = FEcrypt.generate_evaluation_key(my_context, weight_vector, secret_key)
        ev_neg.append(evaluation_key)

        # Now encrypt some data
        data = int_inputs

        data_vector = FEcrypt.create_mpz_vector(data)

        ciphertext = FEcrypt.encrypt(my_context, public_key, data_vector)
        cipher_list_neg.append(ciphertext)
    enc=[[cipher_list_pos,weights_list_pos,ev_pos],[cipher_list_neg,weights_list_neg,ev_neg],contex]
    #print("ev",len(ev_pos),len(ev_neg))


    return enc


