import numpy as np
def shift_to_right(X,fac_in,num_deci):
    """
    :param X:
    :param faktor:
    :return:
   shifts the comma to the right depending on the factor
    """
    m_b_c = X * fac_in
    rounded= np.around(m_b_c, decimals=num_deci)

    m_b_c2int = rounded.astype(int)
    return m_b_c2int


def int2float(x,fac2float):
    """
    change from int to float depending on the factor
"""
    x_float =np.asarray(x)/ fac2float
    return x_float

