import numpy
from convert import shift_to_right


def float2int_input(inputs,fac_in,num_deci):
    """     convert input type from float to int
    """
    # go through all records in the training data set

    int_inputs = shift_to_right(inputs, fac_in,num_deci)
    int_input_list= int_inputs.tolist()


    return int_input_list
