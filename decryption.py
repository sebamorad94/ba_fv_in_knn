import pickle
import FEcrypt
import numpy as np

# weights data
with open("wih.p", "rb") as file:
    wih = pickle.load(file)
def dec(l,hiddennodes):
    #l=[positive_data,negative_data,context_data] l ist the result of the function enc(split_weight, int_inputs,hiddennodes)
    # in encryption.py file
    #positive_data=[positive_ciphertext, positive_weights, positive_evalution Key]
    #negative_data=[negative_ciphertext, negative_weights, negative_evalution Key]

    positive_data=l[0]
    negative_data=l[1]
    context_data=l[2]
    dec_res_list_pos = []
    dec_res_list_neg = []
    for i in range(0,hiddennodes):
        dec_result = FEcrypt.decrypt(positive_data[0][i], positive_data[1][i], context_data[0], positive_data[2][i])
        dec_res_list_pos.append(dec_result)
        dec_result = FEcrypt.decrypt(negative_data[0][i], negative_data[1][i], context_data[0], negative_data[2][i])
        dec_res_list_neg.append(dec_result)
    a = ((np.asarray(dec_res_list_pos) - np.asarray(dec_res_list_neg)))
    x = a.tolist()
    return x

