cimport libc.stdlib


########################################IMPORTANT STRUCTURES########################################
cdef extern from "gmp.h":
    ctypedef unsigned long int	mp_limb_t

    ctypedef struct __mpz_struct:
        int _mp_alloc			#Number of *limbs* allocated and pointed to by the _mp_d field.
        int _mp_size			#abs(_mp_size) is the number of limbs the last field points to.  If _mp_size is negative this is a negative number.
        mp_limb_t *_mp_d		#Pointer to the limbs.

    ctypedef __mpz_struct mpz_t[1]



cdef extern from "defines.h":
    ctypedef struct cfe_ddh:
        size_t l
        mpz_t bound
        mpz_t g
        mpz_t p

    ctypedef struct cfe_vec:
        mpz_t *vec
        size_t size

    ctypedef struct keys:
        cfe_vec secret_key
        cfe_vec public_key


########################################IMPORTED C-FUNCTIONS########################################
cdef extern from "py_fer_interface.h":
    cfe_ddh py_parameter_set(int vector_length, int coordinate_bound, int modulus_len, cfe_ddh context)
    char* py_parameter_print(cfe_ddh context)
    keys py_generate_keys(cfe_ddh context)
    cfe_vec py_secret_key_get(keys both)
    cfe_vec py_public_key_get(keys both)
    char* mpzvec_to_bigstring(cfe_vec v)
    cfe_vec py_vector_from_ints(int* elements, int length)
    void py_evaluation_keygen(cfe_vec func, cfe_vec secret_key, cfe_ddh context, mpz_t fe_key)
    char* py_number_print(mpz_t num)
    cfe_vec py_encrypt(cfe_vec data ,cfe_vec public_key, cfe_ddh context)
    unsigned long int py_decrypt(cfe_vec ciphertext, cfe_vec func, cfe_ddh context, mpz_t evaluation_key)
    void py_vector_delete(cfe_vec fe_vector)
    void mpz_delete(mpz_t num)
    void context_delete(cfe_ddh context)
    void py_free_string(char* str)
    cfe_ddh py_context_load(long l, char* bound, char* g, char* p, cfe_ddh context)
    void str_to_mpz(char* str, mpz_t result)


########################################PYTHON WRAPPER CLASSES########################################
cdef class mpz:
    cdef mpz_t number

    def __del__(self):
        mpz_delete(self.number)
        pass

cdef class parameter:
    cdef cfe_ddh param

    def __del__(self):
        context_delete(self.param)
        pass

cdef class mpz_vec:
    cdef cfe_vec vect

    def __del__(self):
        py_vector_delete(self.vect)
        pass

cdef class both_keys:
    cdef keys key

    def __del__(self):
        py_vector_delete(self.key.secret_key)
        py_vector_delete(self.key.public_key)
        pass


    
########################################PYTHON EXPORTED METHODS########################################
def create_context(vector_length:int,coordinate_bound:int,keylength:int):
    '''
    Creates a parameter set needed for encryption routines
    vector_length=number of elements in one vector
    coordinate_bound=maximum value a vector element or the decryption result can have
    keylength=encryption keylength, given in bits
    return value: parameter-object
    '''
    param=parameter()
    param.param=py_parameter_set(vector_length, coordinate_bound, keylength, param.param)
    return param

def context_to_string(context:parameter):
    '''
    Writes all encryption parameters to a string
    return value:string
    '''
    c_str=py_parameter_print(context.param)
    py_string=str(c_str,'utf-8')
    py_free_string(c_str)
    return py_string

def context_load(vector_length, bound, generator, prime):
    param=parameter()
    #Convert BOUND, GENERATOR and PRIME PARAMETER TO C
    cdef Py_ssize_t count = len(bound)
    cdef char* bound_arr = <char*>libc.stdlib.malloc(sizeof(char) * (count+1))
    for i, value in enumerate(bound):
        bound_arr[i] = ord(value)
    bound_arr[-1]=0
    count=len(generator)
    cdef char* generator_arr = <char*>libc.stdlib.malloc(sizeof(char) * (count+1))
    for i, value in enumerate(generator):
        generator_arr[i] = ord(value)
    generator_arr[-1]=0
    count=len(prime)
    cdef char* prime_arr = <char*>libc.stdlib.malloc(sizeof(char) * (count+1))
    for i, value in enumerate(prime):
        prime_arr[i] = ord(value)
    prime_arr[-1]=0
    param.param=py_context_load(vector_length, bound_arr, generator_arr, prime_arr, param.param)
    libc.stdlib.free(bound_arr)
    libc.stdlib.free(generator_arr)
    libc.stdlib.free(prime_arr)
    return param


def vector_to_string(vect:mpz_vec):
    '''
    Writes contents of a vector of big integers to a list of strings
    return value:list of strings
    '''
    c_str=mpzvec_to_bigstring(vect.vect)
    py_string=str(c_str,'utf-8')
    py_free_string(c_str)
    #We have to split the string since all elements are divided by newline
    py_string=py_string.split("\n")
    #Remove last empty element
    py_string=py_string[:-1]
    return py_string

def create_mpz_vector(integer_list:list):
    '''
    Creates a vector of big integers from integer list
    return value:mpz_vector
    '''
    vector=mpz_vec()
    cdef int i, value
    cdef Py_ssize_t count = len(integer_list)
    cdef int* arr = <int*>libc.stdlib.malloc(sizeof(int) * count)
    for i, value in enumerate(integer_list):
        arr[i] = value
    vector.vect=py_vector_from_ints(arr, count)
    libc.stdlib.free(arr)
    return vector


def keygen(context:parameter):
    '''
    Generates a pair of secret and public key
    return value: [secret key, public key]
    '''
    keyset=both_keys()
    keyset.key=py_generate_keys(context.param)
    secret_key=mpz_vec()
    public_key=mpz_vec()
    secret_key.vect=py_secret_key_get(keyset.key)
    public_key.vect=py_public_key_get(keyset.key)
    return [secret_key, public_key]

def generate_evaluation_key(context:parameter, funct:mpz_vec, secret_key:mpz_vec):
    '''
    Generates an evaluation key tailored to the function(vector) and secret key
    return value:mpz number as evaluation key, yes evaluation key is a single number
    Warning: generation of multiple evaluation keys tailored to the same secret key can massively reduce security
    return value: mpz number
    '''
    eval_key=mpz()
    py_evaluation_keygen(funct.vect, secret_key.vect, context.param, eval_key.number)
    return eval_key

def number_to_string(number:mpz):
    '''
    Writes evaluation key as a string
    return value: string
    '''
    c_str=py_number_print(number.number)
    py_string=str(c_str,'utf-8')
    py_free_string(c_str)
    return py_string

def string_to_number(mpz_str):
    cdef Py_ssize_t count = len(mpz_str)
    cdef char* mpz_str_c = <char*>libc.stdlib.malloc(sizeof(char) * (count+1))
    for i, value in enumerate(mpz_str):
        mpz_str_c[i] = ord(value)
    mpz_str_c[-1]=0
    result_mpz=mpz()
    str_to_mpz(mpz_str_c,result_mpz.number)
    libc.stdlib.free(mpz_str_c)
    return result_mpz

def CreateMpzVecFromStrings(Strings):
    """
    Same as String to number, but with an additional loop and without packaging to python-type
    """
    cdef cfe_vec publickeyvect
    cdef Py_ssize_t count
    cdef char* mpz_str_c 
    publickeyvect.size=len(Strings)
    publickeyvect.vec=<mpz_t*>libc.stdlib.malloc(sizeof(mpz_t) * (publickeyvect.size))
    for i, value in enumerate(Strings):
        count = len(value)
        mpz_str_c = <char*>libc.stdlib.malloc(sizeof(char) * (count+1))
        for n, character in enumerate(value):
            mpz_str_c[n] = ord(character)
            mpz_str_c[-1]=0
        str_to_mpz(mpz_str_c,publickeyvect.vec[i])
        libc.stdlib.free(mpz_str_c)
    public_key=mpz_vec()
    public_key.vect=publickeyvect
    return public_key
        
    

def encrypt(context:parameter, public_key:mpz_vec, data:mpz_vec):
    '''
    Encrypts a data vector
    return value: mpz_vector
    '''
    ciphertext=mpz_vec()
    ciphertext.vect=py_encrypt(data.vect ,public_key.vect, context.param)
    return ciphertext

def decrypt(ciphertext:mpz_vec, func:mpz_vec, context:parameter, evaluation_key:mpz):
    '''
    Evaluates the function on encrypted data
    return value: integer (result)
    '''
    return py_decrypt(ciphertext.vect, func.vect, context.param, evaluation_key.number)
