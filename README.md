# Privatsphärenwahrung in künstlichen neuronalen Netzen  mittels funktionaler Verschlüsselung



## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
* the code consists of a simple artificial neural network for recognition of handwritten numbers, consisting of 3 layers of input, hidden, output layer and functional encryption library is FEcrypt.
* Test case graphs can be found in the graphs file.

## Technologies
* python - version 3.0
* Numpy
* Scipy
* GMP
* Cython
## Setup
`python3 setup.py`

To-do list:
* to train the network 
run the script `train.py`
You can change the number of parameters Neuron Count and Learning Rate as desired
* to test the unencrypted network run the script `python3 test_without_encryption.py`
* to test the encrypted network run the script `python3 test_with_encryption.py`
* The files that generate the graphs have the same names as the images in the bachelor thesis