#ifndef PY_FER_INTERFACE_INCLUDED
#define PY_FER_INTERFACE_INCLUDED
#include "enc_decrypt.h"
#define DEBUG   1


cfe_ddh py_parameter_set(int vector_length, int coordinate_bound, int modulus_len, cfe_ddh context);
char* py_parameter_print(cfe_ddh context);                      
keys py_generate_keys(cfe_ddh context);
cfe_vec py_secret_key_get(keys both);
cfe_vec py_public_key_get(keys both);
char* mpzvec_to_bigstring(cfe_vec v);                               
cfe_vec py_vector_from_ints(int* elements, int length);
void py_evaluation_keygen(cfe_vec func, cfe_vec secret_key, cfe_ddh context, mpz_t fe_key);
char* py_number_print(mpz_t num);
cfe_vec py_encrypt(cfe_vec data ,cfe_vec public_key, cfe_ddh context);
unsigned long int py_decrypt(cfe_vec ciphertext, cfe_vec func, cfe_ddh context, mpz_t evaluation_key);
void py_vector_delete(cfe_vec fe_vector);
void mpz_delete(mpz_t num);
void context_delete(cfe_ddh context);
void py_free_string(char* str);
void str_to_mpz(char* str, mpz_t result);
cfe_ddh py_context_load(long l, char* bound, char* g, char* p,cfe_ddh context);
#endif
