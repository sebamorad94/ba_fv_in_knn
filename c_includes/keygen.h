#ifndef KEYGEN_H_INCLUDED
#define KEYGEN_H_INCLUDED
#include "helpers.h"

cfe_error cfe_elgamal_init(cfe_elgamal *key, size_t modulus_len);
void cfe_elgamal_free(cfe_elgamal *key);
cfe_error cfe_ddh_init(cfe_ddh *s, size_t l, size_t modulus_len, mpz_t bound);
void cfe_ddh_free(cfe_ddh *s);
void cfe_ddh_master_keys_init(cfe_vec *msk, cfe_vec *mpk, cfe_ddh *s);
void cfe_ddh_generate_master_keys(cfe_vec *msk, cfe_vec *mpk, cfe_ddh *s);
cfe_error cfe_ddh_derive_key(mpz_t res, cfe_ddh *s, cfe_vec *msk, cfe_vec *y);

#endif // KEYGEN_H_INCLUDED