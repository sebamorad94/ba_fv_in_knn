#ifndef ENC_DECRYPT_H_INCLUDED
#define ENC_DECRYPT_H_INCLUDED
#include "keygen.h"
cfe_error cfe_ddh_encrypt(cfe_vec *ciphertext, cfe_ddh *s, cfe_vec *x, cfe_vec *mpk);
cfe_error cfe_ddh_decrypt(mpz_t res, cfe_ddh *s, cfe_vec *ciphertext, mpz_t key, cfe_vec *y);
void debug_break();
#endif