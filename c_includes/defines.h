#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <assert.h>
#include <stdio.h>
#include <gmp.h>
#include <sodium.h>
#include "uthash.h"
#define DEBUG 1


typedef enum cfe_error
{
    CFE_ERR_NONE = 0,
    CFE_ERR_INIT,
    CFE_ERR_MALFORMED_PUB_KEY,
    CFE_ERR_MALFORMED_SEC_KEY,
    CFE_ERR_MALFORMED_FE_KEY,
    CFE_ERR_MALFORMED_CIPHER,
    CFE_ERR_MALFORMED_INPUT,
    CFE_ERR_BOUND_CHECK_FAILED,
    CFE_ERR_DLOG_NOT_FOUND,
    CFE_ERR_DLOG_CALC_FAILED,
    CFE_ERR_PRIME_GEN_FAILED,
    CFE_ERR_PARAM_GEN_FAILED,
    CFE_ERR_PUB_KEY_GEN_FAILED,
    CFE_ERR_SEC_KEY_GEN_FAILED,
    CFE_ERR_PRECONDITION_FAILED,
    CFE_ERR_INSUFFICIENT_KEYS,
    CFE_ERR_CORRUPTED_BOOL_EXPRESSION,
    CFE_ERR_NO_SOLUTION_EXISTS,
    CFE_ERR_NO_INVERSE,
} cfe_error;

typedef struct cfe_elgamal
{
    mpz_t p;                        // Modulus
    mpz_t g;                        // Generator of the cyclic group
    mpz_t q;                        // (p - 1) / 2, i.e. order of g
} cfe_elgamal;

typedef struct cfe_ddh
{
    size_t l;
    mpz_t bound;
    mpz_t g;
    mpz_t p;
} cfe_ddh;

typedef struct cfe_vec
{
    mpz_t *vec;                    // pointer to the first integer
    size_t size;
} cfe_vec;

typedef struct keys
{
    cfe_vec secret_key;                    // pointer to the first integer
    cfe_vec public_key;
} keys;

typedef struct bigint_hash
{
    mpz_t key;
    mpz_t val;
    UT_hash_handle hh;
} bigint_hash;
#endif // DEFINES_H_INCLUDED
