#ifndef HELPERS_H_INCLUDED
#define HELPERS_H_INCLUDED
#include "defines.h"


void *cfe_malloc(size_t size);
void cfe_vec_init(cfe_vec *v, size_t size);
void cfe_vec_inits(size_t size, cfe_vec *v, ...);
void cfe_vec_set(cfe_vec *v, mpz_t el, size_t i);
void cfe_vec_get(mpz_t res, cfe_vec *v, size_t i);
bool cfe_vec_check_bound(cfe_vec *v, mpz_t bound);
void cfe_vec_dot(mpz_t res, cfe_vec *v1, cfe_vec *v2);
void cfe_vec_free(cfe_vec *v);
cfe_error cfe_get_prime(mpz_t res, size_t bits, bool safe);
void cfe_uniform_sample(mpz_t res, mpz_t upper);
void cfe_uniform_sample_range(mpz_t res, mpz_t min, mpz_t max);
void cfe_uniform_sample_range_i_mpz(mpz_t res, int min, mpz_t max);
cfe_error cfe_baby_giant(mpz_t res, mpz_t h, mpz_t g, mpz_t p, mpz_t _order, mpz_t bound);
cfe_error cfe_baby_giant_with_neg(mpz_t res, mpz_t h, mpz_t g, mpz_t p, mpz_t _order, mpz_t bound);
#endif // HELPERS_H_INCLUDED
