from convert import  shift_to_right
import pickle
import numpy as np

with open("wih.p", "rb") as file:
    wih = pickle.load(file)


#zwei Nachkommazahlen
def float2int_weight(wih,factor_weight,num_deci):
    """
    convert type of weight matrix from float to int
    """
    int_weight= shift_to_right(wih,factor_weight,num_deci)
    #print("num_deci",num_deci)

    return int_weight

gewichte=[]

def weight_sum():
    for i in range(0, wih.shape[0]):
        s = np.sum(wih[i])
        i= s.astype(int)
        gewichte.append(i)
    p=[abs(n) for n in gewichte]

    return max(p)
