#include "py_fer_interface.h"

cfe_ddh py_parameter_set(int vector_length, int coordinate_bound, int modulus_len, cfe_ddh context)
{
    mpz_t bound;
    mpz_init(bound);
    mpz_set_ui(bound, coordinate_bound);
    cfe_ddh_init(&context, vector_length, modulus_len, bound);
    mpz_clear(bound);
    return context;
}

char* py_parameter_print(cfe_ddh context)
{
    char*   ret;
    int     numbytes;
    numbytes = gmp_snprintf( (char*)NULL, 0, "vector_length=%ul\ncoordinate_bound=%Zd\ngenerator=%Zd\nmodulus=%Zd",context.l,context.bound,context.g,context.p);
    ret = (char*)malloc( ( numbytes + 1 ) * sizeof( char ) );
    gmp_sprintf( ret, "vector_length=%ul\ncoordinate_bound=%Zd\ngenerator=%Zd\nmodulus=%Zd",context.l,context.bound,context.g,context.p);
    return ret;
}

keys py_generate_keys(cfe_ddh context)
{
    keys keyset;
    cfe_ddh_master_keys_init(&(keyset.secret_key), &(keyset.public_key), &context);
    cfe_ddh_generate_master_keys(&(keyset.secret_key), &(keyset.public_key), &context);
    return keyset;
}

cfe_vec py_secret_key_get(keys both)
{
    return both.secret_key;
}

cfe_vec py_public_key_get(keys both)
{
    return both.public_key;
}

char* mpzvec_to_bigstring(cfe_vec v)
{
    /*rewritten*/

    char* result_string;
    char* temp_string;

    //Count bytes
    int total_bytes = 0;
    int max_numbersize = 0;
    int temp_size = 0;
    //Simulate the printing
    for (unsigned long i=0; i<v.size; i++)
    {
    temp_size = gmp_snprintf((char*)NULL, 0, "%Zd\n",v.vec[i]);
    if (temp_size > max_numbersize) max_numbersize=temp_size;
    total_bytes=total_bytes+temp_size;
    }

    //allocate two buffer
    result_string = (char*)malloc((total_bytes+1) * sizeof(char));
    temp_string = (char*)malloc((max_numbersize+1) * sizeof(char));
    //Set first byte to zero to represent an empty string
    result_string[0]='\0';
    //print to a string and concat results
    for (unsigned long i=0; i<v.size; i++)
    {
        gmp_sprintf(temp_string, "%Zd\n",v.vec[i]);
        strcat(result_string,temp_string);
    }
    free(temp_string);
    return result_string;
}

cfe_vec py_vector_from_ints(int* elements, int length)
{
    mpz_t temp;
    mpz_init(temp);
    cfe_vec vect;
    cfe_vec_init(&vect,length);
    for (int i=0; i<length; i++)
    {
        mpz_set_ui(temp,elements[i]);
        cfe_vec_set(&vect,temp,i);
    }
    mpz_clear(temp);
    return vect;
}

void py_vector_delete(cfe_vec fe_vector)
{
    cfe_vec_free(&fe_vector);
}

void mpz_delete(mpz_t num)
{
    mpz_clear(num);
}

void context_delete(cfe_ddh context)
{
    cfe_ddh_free(&context);
}


void py_evaluation_keygen(cfe_vec func, cfe_vec secret_key, cfe_ddh context, mpz_t fe_key)
{
    mpz_init(fe_key);
    cfe_ddh_derive_key(fe_key, &context, &secret_key, &func);
}


char* py_number_print(mpz_t num)
{
    char*   ret;
    int     numbytes;
    numbytes = gmp_snprintf( (char*)NULL, 0, "%Zd",num);
    ret = (char*)malloc( ( numbytes + 1 ) * sizeof( char ) );
    gmp_sprintf( ret, "%Zd",num);
    return ret;
}

void py_free_string(char* str)
{
    free(str);
}


cfe_vec py_encrypt(cfe_vec data ,cfe_vec public_key, cfe_ddh context)
{
    cfe_vec ciphertext;
    cfe_vec_init(&ciphertext, (&context)->l + 1);
    cfe_ddh_encrypt(&ciphertext, &context, &data, &public_key);
    return ciphertext;
}

unsigned long int py_decrypt(cfe_vec ciphertext, cfe_vec func, cfe_ddh context, mpz_t evaluation_key)
{
    mpz_t temp_result;
    mpz_init(temp_result);
    cfe_ddh_decrypt(temp_result, &context, &ciphertext, evaluation_key, &func);
    gmp_printf("-%Zd-",temp_result);
    long int result=mpz_get_ui(temp_result);
    mpz_clear(temp_result);
    return result;
}

cfe_ddh py_context_load(long l, char* bound, char* g, char* p, cfe_ddh context)
{
    str_to_mpz(g,context.g);
    str_to_mpz(p,context.p);
    str_to_mpz(bound,context.bound);
    context.l=l;
    return context;
}

void str_to_mpz(char* str, mpz_t result)
{
    mpz_init(result);
    gmp_sscanf(str,"%Zd", result);
}
