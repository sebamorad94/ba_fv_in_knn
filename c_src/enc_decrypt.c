#include "enc_decrypt.h"


cfe_error cfe_ddh_encrypt(cfe_vec *ciphertext, cfe_ddh *s, cfe_vec *x, cfe_vec *mpk)
{
    if (!cfe_vec_check_bound(x, s->bound)) { return CFE_ERR_BOUND_CHECK_FAILED;}
    mpz_t r, ct, t1, t2;
    mpz_inits(r, ct, t1, t2, NULL);
    cfe_uniform_sample_range_i_mpz(r, 1, s->p);
    mpz_powm(ct, s->g, r, s->p);
    cfe_vec_set(ciphertext, ct, 0);
    for (size_t i = 0; i < s->l; i++)
	{
        cfe_vec_get(t1, mpk, i);
        mpz_powm(t1, t1, r, s->p);
        cfe_vec_get(t2, x, i);
        mpz_powm(t2, s->g, t2, s->p);
        mpz_mul(ct, t1, t2);
        mpz_mod(ct, ct, s->p);
        cfe_vec_set(ciphertext, ct, i + 1);
	}
    mpz_clears(r, ct, t1, t2, NULL);
    return CFE_ERR_NONE;
}


void debug_break(char* number)
{
volatile int i;
gmp_printf("EINGABE ZUM WEITERMACHEN %s\n",number);
    fflush(stdin);
    gmp_scanf("%d",&i);
    fflush(stdin);
}

cfe_error cfe_ddh_decrypt(mpz_t res, cfe_ddh *s, cfe_vec *ciphertext, mpz_t key, cfe_vec *y)
{
    //debug_break("1");
    if (!cfe_vec_check_bound(y, s->bound)) { return CFE_ERR_BOUND_CHECK_FAILED;}
    mpz_t num, ct, t1, denom, denom_inv, r, order, bound;
    mpz_inits(num, ct, t1, denom, denom_inv, r, order, bound, NULL);
    mpz_set_ui(num, 1);
    for (size_t i = 1; i < ciphertext->size; i++)
	{
        cfe_vec_get(ct, ciphertext, i);
        cfe_vec_get(r, y, i - 1);
        mpz_powm(t1, ct, r, s->p);
        mpz_mul(num, num, t1);
        mpz_mod(num, num, s->p);
	}
    cfe_vec_get(ct, ciphertext, 0);
    mpz_powm(denom, ct, key, s->p);
    mpz_invert(denom_inv, denom, s->p);
    mpz_mul(r, denom_inv, num);
    mpz_mod(r, r, s->p);
    mpz_sub_ui(order, s->p, 1);
    mpz_pow_ui(bound, s->bound, 2);
    mpz_mul_ui(bound, bound, s->l);
    //debug_break("2");
    cfe_error err = cfe_baby_giant_with_neg(res, r, s->g, s->p, order, bound);
    mpz_clears(num, ct, t1, denom, denom_inv, r, order, bound, NULL);
    return err;
}
