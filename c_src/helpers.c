#include "helpers.h"
uint64_t small_primes[15] = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53};
uint64_t small_primes_product = 16294579238595022365u;

void *cfe_malloc(size_t size)
{
    void *ptr = malloc(size);

    if (size != 0 && ptr == NULL) {
        perror("Failed to allocate memory");
        abort();
    }

    return ptr;
}

void cfe_vec_init(cfe_vec *v, size_t size)
{
    v->size = size;
    v->vec = (mpz_t *) cfe_malloc(size * sizeof(mpz_t));
    for (size_t i = 0; i < size; i++) {mpz_init(v->vec[i]);}
}

void cfe_vec_inits(size_t size, cfe_vec *v, ...)
{
    va_list ap;
    va_start (ap, v);
    while (v != NULL)
    {
        cfe_vec_init(v, size);
        v = va_arg (ap, cfe_vec*);
    }
    va_end (ap);
}

void cfe_vec_set(cfe_vec *v, mpz_t el, size_t i)
{
    assert (i < v->size);
    mpz_set(v->vec[i], el);
}

void cfe_vec_get(mpz_t res, cfe_vec *v, size_t i)
{
    assert (i < v->size);
    mpz_set(res, v->vec[i]);
}

bool cfe_vec_check_bound(cfe_vec *v, mpz_t bound)
{
    for (size_t i = 0; i < v->size; i++)
    {
        if (mpz_cmp(v->vec[i], bound) >= 0) {return false;}
    }
    return true;
}

void cfe_vec_dot(mpz_t res, cfe_vec *v1, cfe_vec *v2)
{
    assert(v1->size == v2->size);
    mpz_t prod;
    mpz_init(prod);
    mpz_set_si(res, 0);                                 // set it to 0, in case it already holds some value != 0
    for (size_t i = 0; i < v1->size; i++)
    {
        mpz_mul(prod, v1->vec[i], v2->vec[i]);
        mpz_add(res, res, prod);
    }
    mpz_clear(prod);
}

void cfe_vec_free(cfe_vec *v)
{
    for (size_t i = 0; i < v->size; i++) {mpz_clear(v->vec[i]);}
    free(v->vec);
}

cfe_error cfe_get_prime(mpz_t res, size_t bits, bool safe)
{
    if (bits < 2) {return CFE_ERR_PRECONDITION_FAILED;}
    // if we are generating a safe prime, decrease the number of bits by 1
    // as we are actually generating a germain prime and then modifying it to be safe
    // the safe prime will have the correct amount of bits
    size_t n_bits = safe ? bits - 1 : bits;
    size_t n_bytes = (bits + 7) / 8;
    uint8_t *bytes = (uint8_t *) cfe_malloc(n_bytes * sizeof(uint8_t));
    size_t b = n_bits % 8;
    if (b == 0) {b = 8;}
    mpz_t p, p_safe, big_mod;
    mpz_inits(p, p_safe, big_mod, NULL);
    while (true)
    {
        randombytes_buf(bytes, n_bytes);
        bytes[0] &= (uint8_t) ((1 << b) - 1);
        if (b >= 2) {bytes[0] |= 3 << (b - 2);}
        else
        {
            bytes[0] |= 1;
            if (n_bytes > 1) {bytes[1] |= 0x80;}
        }
        bytes[n_bytes - 1] |= 1;
        mpz_import(p, n_bytes, 1, 1, 0, 0, bytes);
        uint64_t mod = mpz_mod_ui(big_mod, p, small_primes_product);
        for (uint64_t delta = 0; delta < (1 << 20); delta += 2)
        {
            uint64_t m = mod + delta;
            bool candidate = true;
            for (size_t i = 0; i < 15; i++)
            {
                uint64_t prime = small_primes[i];
                if (m % prime == 0 && (n_bits > 6 || m != prime))
                {
                    candidate = false;
                    break;
                }
                if (safe)
                {
                    uint64_t m1 = (2 * m + 1) % small_primes_product;
                    if (m1 % prime == 0 && (n_bits > 6 || m1 != prime))
                    {
                        candidate = false;
                        break;
                    }
                }

            }
            if (candidate)
            {
                if (delta > 0)
                {
                    mpz_set_ui(big_mod, delta);
                    mpz_add(p, p, big_mod);
                }
                if (safe)
                {
                    mpz_mul_ui(p_safe, p, 2);
                    mpz_add_ui(p_safe, p_safe, 1);
                }
                break;
            }
        }
        if (mpz_probab_prime_p(p, 10) && mpz_sizeinbase(p, 2) == n_bits)
        {
            if (!safe)
            {
                if (mpz_probab_prime_p(p, 30)) {break;}
            }
            else if (mpz_probab_prime_p(p_safe, 50) && mpz_probab_prime_p(p, 30)) {break;}
        }
    }
    if (safe) {mpz_set(res, p_safe);} else {mpz_set(res, p);}
    free(bytes);
    mpz_clears(p, p_safe, big_mod, NULL);
    if (mpz_sizeinbase(res, 2) != bits) {return CFE_ERR_PRIME_GEN_FAILED;}
    return CFE_ERR_NONE;
}

void cfe_uniform_sample(mpz_t res, mpz_t upper)
{
    size_t n_bits = mpz_sizeinbase(upper, 2);                                               // determine the size of buffer to read random bytes in and allocate it
    size_t n_bytes = ((n_bits - 1) / 8) + 1;
    uint8_t *rand_bytes = (uint8_t *) cfe_malloc(n_bytes * sizeof(uint8_t));
    mpz_t max;
    mpz_init(max);
    mpz_ui_pow_ui(max, 2, n_bits);                                                          // calculate max unsigned number that we can represent with the given number of bits
    while (1)
    {
        randombytes_buf(rand_bytes, n_bytes);                                               // get random bytes
        mpz_import(res, n_bytes, 1, 1, 0, 0, rand_bytes);                                   // make a big integer number from random bytes result is always positive
        if (mpz_cmp(res, max) >= 0) {mpz_fdiv_r(res, res, max);}                            // random number too big, divide it
        if (mpz_cmp(res, upper) < 0) {break;}                                               // if we're below the upper bound, we have a valid random number
    }
    free(rand_bytes);
    mpz_clear(max);
}

void cfe_uniform_sample_range(mpz_t res, mpz_t min, mpz_t max)
{
    mpz_t max_sub_min;
    mpz_init(max_sub_min);
    mpz_sub(max_sub_min, max, min);
    cfe_uniform_sample(res, max_sub_min);                                                   // sets res to be from [0, max-min)
    mpz_add(res, res, min);                                                                 // sets res to be from [min, max)
    mpz_clear(max_sub_min);
}

void cfe_uniform_sample_range_i_mpz(mpz_t res, int min, mpz_t max)
{
    mpz_t min_z;
    mpz_init_set_si(min_z, min);
    cfe_uniform_sample_range(res, min_z, max);
    mpz_clear(min_z);
}


cfe_error cfe_baby_giant(mpz_t res, mpz_t h, mpz_t g, mpz_t p, mpz_t _order, mpz_t bound)
{
    if (_order == NULL && mpz_probab_prime_p(p, 20) == 0) {return CFE_ERR_DLOG_CALC_FAILED;}
    mpz_t order, m, x, i, z, tmp;
    mpz_inits(order, m, x, i, z, tmp, NULL);
    cfe_error err = CFE_ERR_DLOG_NOT_FOUND;
    if (_order == NULL) {mpz_sub_ui(order, p, 1);} else {mpz_set(order, _order);}
    if (bound != NULL) {mpz_sqrt(m, bound);} else {mpz_sqrt(m, order);}
    mpz_add_ui(m, m, 1);
    mpz_set_ui(x, 1);
    bigint_hash *T = NULL;                                                                              // the hash table
    bigint_hash *t, *u;                                                                                 // reusable pointer for hash table entries + a temporary variable for clearing later
    for (mpz_set_ui(i, 0); mpz_cmp(i, m) < 0; mpz_add_ui(i, i, 1))
        {
        t = (bigint_hash *) cfe_malloc(sizeof(bigint_hash));                                            // store T[x] = i create a struct t and store the key and value the key is actually the internal contents of a mpz_t, the array and the minimal possible length
        mpz_init_set(t->key, x);
        mpz_init_set(t->val, i);
        HASH_ADD_KEYPTR(hh, T, t->key->_mp_d, t->key->_mp_alloc * sizeof(mp_limb_t), t);                // the key is a pointer the the array and the length in bytes
        mpz_mul(x, x, g);
        mpz_mod(x, x, p);
    }
    mpz_invert(z, g, p);
    mpz_powm(z, z, m, p);
    mpz_set(x, h);
    for (mpz_set_ui(i, 0); mpz_cmp(i, m) < 0; mpz_add_ui(i, i, 1))
    {
        // get T[x], the value in X needs to be assigned to a temporary variable when setting a mpz_t's value, the minimal possible amount of memory will be allocated, which ensures that we get the same amount of bytes read from both arrays, which is needed for a match
        mpz_set(tmp, x);
        HASH_FIND(hh, T, tmp->_mp_d, tmp->_mp_alloc * sizeof(mp_limb_t), t);
        if (t != NULL)
        {
            mpz_mul(res, i, m);
            mpz_add(res, res, t->val);
            err = CFE_ERR_NONE;
            break;
        }
        mpz_mul(x, x, z);
        mpz_mod(x, x, p);
    }
    // cleanup iterate through all entries, clear the mpz_t variables, delete the entries and free their allocated memory
    HASH_ITER(hh, T, t, u)
    {
        mpz_clear(t->val);
        mpz_clear(t->key);
        HASH_DEL(T, t);
        free(t);
    }
    free(T);
    mpz_clears(order, m, x, i, z, tmp, NULL);
    return err;
}

// TODO: parallel version?
cfe_error cfe_baby_giant_with_neg(mpz_t res, mpz_t h, mpz_t g, mpz_t p, mpz_t _order, mpz_t bound)
{
    cfe_error err = cfe_baby_giant(res, h, g, p, _order, bound);
    if (err)
    {
        mpz_t g_inv;
        mpz_init(g_inv);
        mpz_invert(g_inv, g, p);
        err = cfe_baby_giant(res, h, g_inv, p, _order, bound);
        if (err == 0) {mpz_neg(res, res);}
        mpz_clear(g_inv);
    }
    return err;
}
