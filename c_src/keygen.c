#include "keygen.h"
cfe_error cfe_elgamal_init(cfe_elgamal *key, size_t modulus_len)
{
    mpz_t x, two, check, g_inv;
    mpz_inits(x, two, check, g_inv, key->p, key->g, key->q, NULL);
    cfe_error err = cfe_get_prime(key->p, modulus_len, true);               // set p to a safe prime
    if (err)
    {
        mpz_clears(key->p, key->g, key->q, NULL);
        goto cleanup;
    }
    mpz_sub_ui(key->q, key->p, 1);
    mpz_div_ui(key->q, key->q, 2);
    mpz_set_ui(two, 2);
    while (true)
	{
        cfe_uniform_sample_range_i_mpz(key->g, 3, key->p);
        mpz_powm(key->g, key->g, two, key->p);                      // make g an element of the subgroup of quadratic residues
        mpz_sub_ui(check, key->p, 1);
        mpz_mod(check, check, key->g);
        if (mpz_cmp_ui(check, 0) == 0) {continue;}                  // additional checks to avoid some known attacks
        mpz_invert(g_inv, key->g, key->p);
        mpz_sub_ui(check, key->p, 1);
        mpz_mod(check, check, g_inv);
        if (mpz_cmp_ui(check, 0) == 0){continue;}
        break;
    }
    cfe_uniform_sample_range(x, two, key->q);                       // x is randomly generated secret key
    cleanup:
    mpz_clears(x, two, check, g_inv, NULL);
    return err;
}



void cfe_elgamal_free(cfe_elgamal *key)                             // Frees the memory allocated for the members of cfe_elgamal struct
{
    mpz_clears(key->g, key->p, key->q, NULL);
}


cfe_error cfe_ddh_init(cfe_ddh *s, size_t l, size_t modulus_len, mpz_t bound)
{
    cfe_elgamal key;
    if (cfe_elgamal_init(&key, modulus_len)) {return CFE_ERR_PARAM_GEN_FAILED;}
    cfe_error err = CFE_ERR_NONE;
    mpz_t check;
    mpz_init(check);
    mpz_pow_ui(check, bound, 2);
    mpz_mul_ui(check, check, l);
    if (mpz_cmp(check, key.p) >= 0)
	{
        err = CFE_ERR_PRECONDITION_FAILED;
        goto cleanup;
	}
    s->l = l;
    mpz_init_set(s->bound, bound);
    mpz_init_set(s->g, key.g);
    mpz_init_set(s->p, key.p);
    cleanup:
    cfe_elgamal_free(&key);
    mpz_clear(check);
    return err;
}

void cfe_ddh_free(cfe_ddh *s)
{
    mpz_clears(s->bound, s->g, s->p, NULL);
}

void cfe_ddh_master_keys_init(cfe_vec *msk, cfe_vec *mpk, cfe_ddh *s)
{
    cfe_vec_inits(s->l, msk, mpk, NULL);
}

void cfe_ddh_generate_master_keys(cfe_vec *msk, cfe_vec *mpk, cfe_ddh *s)
{
    mpz_t x, p_min_1;
    mpz_inits(x, p_min_1, NULL);
    mpz_sub_ui(p_min_1, s->p, 1);
    for (size_t i = 0; i < s->l; i++)
	{
        cfe_uniform_sample_range_i_mpz(x, 2, p_min_1);
        cfe_vec_set(msk, x, i);
        mpz_powm(x, s->g, x, s->p);
        cfe_vec_set(mpk, x, i);
    }
    mpz_clears(x, p_min_1, NULL);
}

cfe_error cfe_ddh_derive_key(mpz_t res, cfe_ddh *s, cfe_vec *msk, cfe_vec *y)
{
    if (!cfe_vec_check_bound(y, s->bound)) {return CFE_ERR_BOUND_CHECK_FAILED;}
    mpz_t p_min_one;
    mpz_init(p_min_one);
    cfe_vec_dot(res, msk, y);
    mpz_sub_ui(p_min_one, s->p, 1);
    mpz_mod(res, res, p_min_one);
    mpz_clear(p_min_one);
    return CFE_ERR_NONE;
}

