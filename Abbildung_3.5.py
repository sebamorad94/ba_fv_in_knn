import csv
import statistics
import time
import FEcrypt
import matplotlib.pyplot as plt
import numpy

from encryption import greate_context
#f = open('Key_time.csv', 'w')
#wr = csv.writer(f)
def keyGen_Time_test(keyLength,durchläufe):
    keyLength_list = []
    laufZeit_list = []
    keyLength_list.append(keyLength)
    for i in range(durchläufe):
        tic = time.perf_counter()
        my_context = greate_context(keyLength)
        [secret_key, public_key] = FEcrypt.keygen(my_context)
        toc = time.perf_counter()
        time_for_KeyGen = toc - tic
        laufZeit_list.append(time_for_KeyGen)
    return laufZeit_list

keyLength_lists = []
laufZeit_lists = []

def get_mean(l):
    test=[statistics.mean(list_l) for list_l in l]
    return test

durchläufe=40
keyLength_list=[128,256,512,1024,2048]
keyLength= 64
#for i in range(2):
    #keyLength=keyLength*2
    #keyLength_list.append(keyLength)
    #zeit =keyGen_Time_test(keyLength,durchläufe)
    #wr.writerow(zeit)
Key_time_file = open("Key_time.csv", 'r')
Key_time_list = Key_time_file.readlines()
Key_time_file.close()

for time in Key_time_list:
    time_list = time.split(',')
    time_list=numpy.asfarray(time_list)
    time_list=time_list.tolist()
    laufZeit_lists.append(time_list)
mw_time=get_mean(laufZeit_lists)
plt.xlabel('Schlüssellänge in Bits',fontsize = 25)
plt.ylabel('Schlüsselgenerierungszeit in Sekunden',fontsize = 25)
d=str(durchläufe)
plt.title("Durchschnitt von 40 Durchläufe",fontsize = 25)
plt.xticks([128,512,1024,2048])
plt.plot(keyLength_list,mw_time)
plt.plot(keyLength_list, mw_time,"bo")
plt.yticks(mw_time)
plt.legend()
plt.show()
#plt.savefig("graphs/pu_pr_Keys_Generation_time.png",dpi=100)

