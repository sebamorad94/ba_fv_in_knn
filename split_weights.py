#um mit negativen Gewichte arbeiten zu können wird jede Zeile in Gewicht Matrix in zwei Vektoren verschachtelt , Beispiel
#Gewichtvektor=(1,-1,2)
#pos_weights=[1,0,2]
#neg_weights=[0,1,0]
import numpy as np


from convert_weight import float2int_weight

arr2lis=[]

def array2list(int_weight):
    """
    convert weight array to list
    """
    for i in int_weight:
        r2l=i.tolist()

        arr2lis.append(r2l)
    return arr2lis
all_weights_lists=[]

def split_we(test):
    """ example: [1,-2,3] = [[1,0,3],[0,2,0]]
        """
    for l in test:
        pos= [p if p>0 else 0 for p in l]
        neg=[n*-1 if n<0 else 0 for n in l]
        all_weights_lists.append([pos,neg])

    return all_weights_lists